package br.qa.andrericardo.airbnb.core;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class BaseTest {
	
	@Rule
	public TestName testName = new TestName();
	
	@AfterClass
	public static void finishClass() {
		
		DriverFactory.killDriver();
	}
	
	@After
	public void tearDown() {
		 
		gerarScreenShot();
		DriverFactory.closeApp();
	}
	
	public void gerarScreenShot() {
		
		File image = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(image, new File("screenshots/"+ testName.getMethodName() +".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void delay(long t) {
		
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
