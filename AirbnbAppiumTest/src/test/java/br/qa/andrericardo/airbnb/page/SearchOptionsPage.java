package br.qa.andrericardo.airbnb.page;

import static br.qa.andrericardo.airbnb.core.DriverFactory.getDriver;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;

import br.qa.andrericardo.airbnb.core.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;


public class SearchOptionsPage extends BasePage{
	
	
	public void selectShortStay() {
		
		// Select Short Stay < 4 weeks
		MobileElement el4 = (MobileElement) getDriver().findElementById("com.airbnb.android:id/2131430235"); 
		el4.click();
	}

	public void scrollEndPage() {
		
		scroll(0.9, 0.1);
		scroll(0.7, 0.1);
	}
	
	public void scrollUpJanuary() {
		
		boolean findMonth = false;
		
		while (!findMonth) {
			
			scrollUpMonth();
			if (existElementByText("janeiro")){
				findMonth = true;
			}else{
				scrollUpMonth();
			}
		}
	}
	
	public void scrollUpMonth() {
		
		scroll(0.9, 0.9);
		scroll(0.8, 0.3);
	}
	
	public void pressSendButton() throws MalformedURLException{
		
		pressSomeKey(AndroidKeyCode.BACK);
		tapCoordinatePoint(0.83, 0.90);
	}

	public void selectDaysRange() {
		
		touchNumber("1");
		touchNumber("31");
		touchButton("Pr�ximo");
	}

	public void touchButton(String btn) {
		
		touchElement("Button", btn);
	}

	public void addLodger(int q_adult, int q_child, int q_baby) {
		
		if (q_adult > 0) {
			touchAddAdult(q_adult, 1);
		}
		if (q_child > 0) {
			touchAddAdult(q_child, 2);
		}
		if (q_baby > 0) {
			touchAddAdult(q_baby, 3);
		}
		touchButton("Buscar");
	}

	// q guest(s)
	public void touchAddAdult(int q, int option) {
		
		List<MobileElement> elements= getDriver().findElementsByAccessibilityId("Aumento");
		
		int count = 0;
		for (MobileElement element: elements) {
			count++ ;
			if (count == option) { // option: Adult | Child | Baby
				for(int i=1; i<=q; i++) { // q clicks
					element.click();
				}
			}
		}
	}

	public void selectPriceMinor() {
		
		tapElement("Pre�o");
		tapElement("Valor m�ximo R$100.000+. Ajust�vel. Deslize o cursor para a frente para aumentar. Deslize o cursor para tr�s para diminuir.");
		slide(0.85, 0.14, 0.57);
		touchButton("Salvar");
	}

	public boolean checkFilterPrice(String txtPrice) {
		
		try {
			MobileElement el = (MobileElement) getDriver().findElementByAccessibilityId(txtPrice);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean checkFirstRental(int value) {
		
		scroll(0.9, 0.1);
		String textRental = findElementContains("TextView", "R$");
	
		return (value >= Integer.valueOf((textRental.substring(textRental.lastIndexOf('$') + 1, textRental.indexOf('/'))).trim()));
	}

	public void selectApartment() {
		
		tapElement("Filtros");
		
		boolean findApartment = false;
		while (!findApartment) {
			scroll(0.8, 0.1);
			if (existElementByText("Apartamento")) {
				touchElement("TextView", "Apartamento");
				findApartment = true;
			}
		}
		
		touchElement("Button", findElementContains("Button", "acomoda��es"));
	}

	public boolean checkApartment() {
		
		int count = 0;
		while (true) {
			scroll(0.8, 0.1);
			if (existElementByText("Apto") || existElementByText("Apt") || existElementByText("Apartamento")) {
				return true;
			}else {
				count++;
				if (count == 10) {
					break;
				}
				continue;
			}
		}
		return false;
	}

}
