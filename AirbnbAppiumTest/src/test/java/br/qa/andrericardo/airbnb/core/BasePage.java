package br.qa.andrericardo.airbnb.core;

import static br.qa.andrericardo.airbnb.core.DriverFactory.getDriver;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.touch.offset.PointOption;

public class BasePage {
	
	public String getTextResult(By by) {
		
		return getDriver().findElement(by).getText();
	}

	public void searchPlace(String placeName) throws MalformedURLException {
		   
		// Touch initial Search field   
		MobileElement el1 = (MobileElement) getDriver().findElementById("com.airbnb.android:id/2131428871");
		el1.click();
		
		// Touch second Search field
		MobileElement el2 = (MobileElement) getDriver().findElementById("com.airbnb.android:id/2131428843");
		el2.sendKeys(placeName);
		
		MobileElement el3 = (MobileElement) getDriver().findElementById("com.airbnb.android:id/title");
		el3.click();
	}
	
	public void openApp(String app) throws MalformedURLException, InterruptedException {
		
		boolean findIconApp = false;
		
		pressSomeKey(AndroidKeyCode.KEYCODE_HOME);
		if (existTxtElement("TextView","Aplic.")) {
			tapCoordinatePoint(0.5, 0.86);
		}
		
		while(!findIconApp){
			try {
				MobileElement el = getDriver().findElementByXPath("//android.widget.FrameLayout[@content-desc=\""+ app +"\"]/android.widget.ImageView");
				findIconApp = true;
				el.click();
			} catch (Exception e) {
				swipeHorizontal(0.1, 0.9);
			}
		}
		
		Thread.sleep(5000);
		
		tapElement("Navegar para cima");
	}
	
	public void tapElement(String item) {
		
		MobileElement el = (MobileElement) getDriver().findElementByAccessibilityId(item);
		el.click();
	}
	
	public void touchAndInput(String item, String option) {
		
		MobileElement element = (MobileElement) getDriver().findElementById("com.airbnb.android:id/2131428871");
		element.click();
		element.sendKeys(option);
	}
	
	public void touchNumber(String txtNum) {
		
		touchElement("TextView", txtNum);
	}
	
	public void touchElement(String typeElement, String txtItem) {
		
		List<MobileElement> elements = getDriver().findElements(By.className("android.widget."+ typeElement + ""));
		
		for (MobileElement element : elements) {
			 if (element.getText().equals(txtItem)) {
			    	element.click();
			    	break;
			    }
		}
	}
	
	public String findElementContains(String typeElement, String txtItem) {
			
			List<MobileElement> elements = getDriver().findElements(By.className("android.widget."+ typeElement + ""));
			
			for (MobileElement element : elements) {
				System.out.println(element.getText());
				 if (element.getText().contains(txtItem)) {
				    	return element.getText();
				    }
			}
			return null;
		}
	
	public boolean existTxtElement(String typeElement, String txtItem) {
		
		List<MobileElement> elements = getDriver().findElements(By.className("android.widget."+ typeElement + ""));	
		return (elements.size() > 0);
	}
	
	public boolean partialTxtElement(String typeElement, String txtItem) {
		
		List<MobileElement> elements = getDriver().findElements(By.className("android.widget."+ typeElement + ""));
			
		for (MobileElement element: elements) {
			if (element.getText().contains(txtItem)) {
				return true;
			}
		}
		return false;
	}
	
	public void pressSomeKey(int key) {
		
		getDriver().pressKeyCode(key);
	}
	
	public boolean getTextTitle(String text) {
		
		MobileElement title = (MobileElement) getDriver().findElementById("com.airbnb.android:id/title");
		
		if (title.getText().length() > 0) {
			return true;
		}else {
			return false;
		}	
	}
	
	public boolean textOccurrenceCount(String text) {
		
		List<MobileElement> elements = getDriver().findElements(By.xpath("//*[@text='"+text+"']"));
		return elements.size() > 0;
	}
	
	public void clickOnText(String text) {
		
		getDriver().findElement(By.xpath("//*[@text='"+text+"']")).click();
	}
	
	public void scroll(double vert_begin, double vert_end) {
		
		Dimension size = getDriver().manage().window().getSize();
		
		int x = size.width / 2;
		int start_y = (int) (size.height * vert_begin);
		int end_y = (int) (size.height * vert_end);
		
		TouchAction touchAction = new TouchAction(getDriver());
	    touchAction.longPress(PointOption.point(x, start_y)).
	    	moveTo(PointOption.point(x, end_y)).
	    	release().perform();
	}
	
	public MobileElement getElementStr(String s) {
		
		List<MobileElement> elements =  getDriver().findElements(By.className("android.widget.TextView"));
		
		for (MobileElement element: elements) {
			if (element.getText().contains(s)) {
				return element;
			}
		}
		return null;		
	}
	
	public void tapListOptions(String option) {

		getDriver().findElement(By.xpath("//android.widget.TextView[@content-desc='"+ option +"']")).click();
	}
	
	public boolean findElementPartialText(String text) {
		
		System.out.println("text: " + text);
		String s = findElementContains("TextView", text);
		System.out.println("s: " + s);
		return (s.contains(text));
	}
	
	//proportional coordinates for tap
	public void tapCoordinatePoint(double ref_x, double ref_y) throws MalformedURLException{
		
		Dimension size = getDriver().manage().window().getSize();
		
		int x = (int) (size.width * ref_x);
		int y = (int) (size.height * ref_y);
		
		TouchAction touchAction = new TouchAction(getDriver());
		touchAction.tap(new PointOption().withCoordinates(x, y)).perform();
	}
	
	public void swipeHorizontal(double hrz_begin, double hrz_end ) {
			
			Dimension size = getDriver().manage().window().getSize();
			
			int y = size.height - 50;
			int start_x = (int) (size.width * hrz_begin);
			int end_x = (int) (size.width * hrz_end);
			
			TouchAction touchAction = new TouchAction(getDriver());
		    touchAction.longPress(PointOption.point(start_x, y)).
		    	moveTo(PointOption.point(end_x, y)).
		    	release().perform();
	}
	
	public void slide(double hrz_begin, double hrz_end, double vert) {
		
		Dimension size = getDriver().manage().window().getSize();
		
		int y = (int) (size.height * vert);
		int start_x = (int) (size.width * hrz_begin);
		int end_x = (int) (size.width * hrz_end);
		
		TouchAction touchAction = new TouchAction(getDriver());
	    touchAction.longPress(PointOption.point(start_x, y)).
	    	moveTo(PointOption.point(end_x, y)).
	    	release().perform();
	}

	public double getVertPosText(String s) {
		
		Dimension size = getDriver().manage().window().getSize();
		
		int vertical = size.height;
		
		MobileElement el = getElementStr(s);
		System.out.println(el.getLocation().y);
		
		return (el.getLocation().y / vertical);
		
	}

}
