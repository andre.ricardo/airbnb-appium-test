package br.qa.andrericardo.airbnb.test;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.Test;

import br.qa.andrericardo.airbnb.core.BasePage;
import br.qa.andrericardo.airbnb.core.BaseTest;
import br.qa.andrericardo.airbnb.page.SearchOptionsPage;

/**
 * @author Andr� Ricardo
 * @email: andrericardo.pro@gmail.com
 * 
 * This testing project was designed aiming to show  
 * my assess skills to the intended QA Engineer position. 
 * 
 */

public class SearchPlacesManausJanuary extends BaseTest{
	
	private SearchOptionsPage search = new SearchOptionsPage();
	private BasePage page = new BasePage();
	
	@Test
	public void test_1_anonymousAccessAirbnb( ) throws MalformedURLException, InterruptedException {
		
		delay(5000);
		page.openApp("Airbnb");
		
		Assert.assertTrue(page.findElementPartialText("Localiza��o, ponto tur�stico ou endere�o"));
		Assert.assertTrue(page.getTextTitle("Podemos estar afastados, mas vamos superar isso juntos."));
	}
	
	@Test
	public void test_2_searchPlaceAsSingle() throws MalformedURLException, InterruptedException {
		
		delay(5000);
		page.openApp("Airbnb");
		page.searchPlace("Manaus");
		search.selectShortStay();
		search.scrollUpJanuary();
		search.selectDaysRange();
		
		search.addLodger(1, 0, 0);
		search.selectPriceMinor();
		
		Assert.assertTrue(search.checkFilterPrice("At� R$790"));
		Assert.assertTrue(search.checkFirstRental(790));
	}
	

	@Test
	public void test_3_searchApartmentWithBaby() throws MalformedURLException, InterruptedException {
		
		delay(5000);
		page.openApp("Airbnb");
		page.searchPlace("Manaus");
		search.selectShortStay();
		search.scrollUpJanuary();
		search.selectDaysRange();
		
		search.addLodger(0, 0, 1);
		search.selectApartment();
		
		Assert.assertTrue(search.checkApartment());
		
	}
	
}