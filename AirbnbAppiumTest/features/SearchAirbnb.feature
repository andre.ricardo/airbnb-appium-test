#language: en
Feature: Search places in Manaus on Airbnb
	As a user
	I want to search place, period and prices
	So I can to know how much I will spend
	
Scenario: Anonymous login

	Given I open Airbnb app
	When I not input Login and Password
	Then I am directed the Home app

Scenario: Search in January as single
	Given I has opened the Airbnb app
	When I input place 'Manaus' 
	And I select 'Encontre um lugar para ficar' option
	And I select month January
	And I only one guest   
	Then I can know the minor price avaible 
	
Scenario: Search in January an apartment with one baby
	Given I has opened the Airbnb app
	When I input place 'Manaus' 
	And I select 'Encontre um lugar para ficar' option
	And I select month 
	And I only one baby
	And I choice apartment
	Then the app shown only prices for apartment 